#!/usr/bin/env bash

# lock/unlock directory recursively
# Author Igor P. Zenkov

# constants

declare -r SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
declare -r SCRIPT_NAME=$(basename "$0")
declare -r SCRIPT_VER='1.0.1 Nov 04, 2019'
declare -r SCRIPT_COMMANDS='lock unlock status version help'

declare -r LOCK_MODE_DIR=500
declare -r UNLOCK_MODE_DIR=755

declare -r LOCK_MODE_FILE=000
declare -r UNLOCK_MODE_FILE=644

declare -r LOCK_MODE_XFILE=100
declare -r UNLOCK_MODE_XFILE=755

# support functions

function assert_args() {
  local want=$1
  local actual=$2
  local fn_help=$3
  # if [ "$want" -ne "$actual" ]; then
  if [ "$actual" -lt "$want" ]; then
    echo "$SCRIPT_NAME: missing required arguments"
    "$fn_help"
    exit 1
  fi
}

function assert_dir() {
  local dname=$1
  if [ "$#" -ne 0 ]; then
    if [ ! -d "$dname" ]; then
      echo "$SCRIPT_NAME: folder '$dname' not found"
      exit 1
    fi
  fi
}

function help_cmd() {
  local cmd
  usage
  for cmd in $@; do
    printf "  $cmd\n"
    cmd=${cmd/-/_} # cmd-1 -> cmd_1
    "usage_$cmd"
  done
}

# functions

function chmod_get() {
  local -r f=$1
  local -r m=$(stat -c '%a' $f)
  printf "%.3i %s\n" "$m" "$f"
}

function chmod_set() {
  local -r m=$1
  local -r f=$2
  chmod "$m" "$f"
  chmod_get "$f"
}

function chmod_set_recursive() {
  local -r dir=$1
  local -r dmod=$2
  local -r fmod=$3
  local -r xmod=$4
  shopt -s globstar
  local -r skip_dname="${dir}/"
  local -r skip_fname="${dir}/$SCRIPT_NAME"
  local f
  for f in "${dir}"/**; do
    [[ -d "$f" && "$f" != "$skip_dname" ]] && chmod_set "$dmod" "$f"
    if [[ -f "$f" && "$f" != "$skip_fname" ]]; then
      if [[ -x "$f" ]]; then
        chmod_set "$xmod" "$f"
      else
        chmod_set "$fmod" "$f"
      fi
    fi
  done
}

function chmod_get_recursive() {
  local -r dir=$1
  shopt -s globstar
  local -r skip_dname="${dir}/"
  local -r skip_fname="${dir}/$SCRIPT_NAME"
  local f
  for f in "${dir}"/**; do
    [[ -d "$f" && "$f" != "$skip_dname" ]] && chmod_get "$f"
    [[ -f "$f" && "$f" != "$skip_fname" ]] && chmod_get "$f"
  done
}

# commands

function cmd_lock() {
  local -r dir=${1%/}
  assert_args 1 $# 'usage_lock'
  assert_dir "${dir}"
  chmod_set_recursive "${dir}" "$LOCK_MODE_DIR" "$LOCK_MODE_FILE" "$LOCK_MODE_XFILE"
}

function cmd_unlock() {
  local -r dir=${1%/}
  assert_args 1 $# 'usage_unlock'
  assert_dir "${dir}"
  chmod_set_recursive "${dir}" "$UNLOCK_MODE_DIR" "$UNLOCK_MODE_FILE" "$UNLOCK_MODE_XFILE"
}

function cmd_status() {
  local -r dir=${1%/}
  assert_args 1 $# 'usage_status'
  assert_dir "${dir}"
  chmod_get_recursive "${dir}"
}

function cmd_version() {
  echo "$SCRIPT_VER"
}

function cmd_help() {
  if [ "$#" -eq 0 ]; then
    help_cmd $SCRIPT_COMMANDS
  else
    help_cmd $@
  fi
} 

# usage

function usage_lock() {
  printf "\n"
  printf "  Usage: $SCRIPT_NAME lock <dir> \n"
  printf "  Where: <dir>       Directory to lock \n"
  printf "   Like: $SCRIPT_NAME lock . \n"
  printf "         $SCRIPT_NAME lock ~/backup \n"
  printf "         $SCRIPT_NAME lock /home/testuser/backup \n"
  printf "\n"
}

function usage_unlock() {
  printf "\n"
  printf "  Usage: $SCRIPT_NAME unlock <dir> \n"
  printf "  Where: <dir>       Directory to unlock \n"
  printf "   Like: $SCRIPT_NAME unlock . \n"
  printf "         $SCRIPT_NAME unlock ~/backup \n"
  printf "         $SCRIPT_NAME unlock /home/testuser/backup \n"
  printf "\n"
}

function usage_status() {
  printf "\n"
  printf "  Usage: $SCRIPT_NAME status <dir> \n"
  printf "  Where: <dir>       Directory to show lock status \n"
  printf "   Like: $SCRIPT_NAME status . \n"
  printf "         $SCRIPT_NAME status ~/backup \n"
  printf "         $SCRIPT_NAME status /home/testuser/backup \n"
  printf "\n"
}

function usage_version() {
  printf "\n"
  printf "  Usage: $SCRIPT_NAME version \n"
  printf "\n"
}

function usage_help() {
  printf "\n"
  printf "  Usage: $SCRIPT_NAME help \n"
  printf "         $SCRIPT_NAME help COMMAND \n"
  printf "   Like: $SCRIPT_NAME help lock \n"
  printf "\n"
}

function usage() {
  printf "$SCRIPT_NAME $SCRIPT_VER \n"
  printf "Usage: $SCRIPT_NAME COMMAND \n\n"
  printf "Commands:\n"
  printf "  lock    Lock directory \n"
  printf "  unlock  Unlock directory \n"
  printf "  status  Directory lock status \n"
  printf "  version Show version \n"
  printf "  help    Show detailed help \n"
  printf "\n"
}

# main

function main() {
  local -r cmd=$1
  local -r cnt=$#
  shift 1
  case $cmd in
    'lock')
      cmd_lock "$@"
      ;;
    'unlock')
      cmd_unlock "$@"
      ;;
    'status')
      cmd_status "$@"
      ;;
    'version')
      cmd_version "$@"
      ;;
    'help')
      cmd_help "$@"
      ;;
    *)
      if [ "$cnt" -eq 0 ]; then
        usage
      else
        echo "Unknown command: $cmd"
      fi
  esac
}

# entry point

main "$@"


