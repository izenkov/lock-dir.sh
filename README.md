# lock-dir.sh 1.0.1

lock-dir.sh is a Bash script to lock/unlock folder recursively

### How does lock works?

Lock command iterates given folder and all files and folders below it recursively
and changes files and folders permissions (mods) according to the following table:

| File System Item | Original | Locked |
| ---------------- | -------- |------- |
| Directory        | Any      | 500    |
| File             | Any      | 000    |
| Executable File  | Any      | 100    |

### How does unlock works?

Unlock command iterates given folder and all files and folders below it recursively
and changes files and folders permissions (mods) according to the following table:

| File System Item | Locked | Unlocked |
| ---------------- | ------ |--------- |
| Directory        | 500    | 755      |
| File             | 000    | 644      |
| Executable File  | 100    | 755      |

### Tested environments:

 - **Ubuntu Linux 18.04 LTS**

### Dependencies:

 - **Bash**

### Usage:

```bash
  ./lock-dir.sh COMMAND
```
### lock-dir.sh commands:

 - **lock**    Lock directory 
 - **unlock**  Unlock directory 
 - **status**  Directory lock status 
 - **version** Show version 
 - **help**    Show detailed help 

### lock-dir.sh usage:

```bash

  lock

  Usage: lock-dir.sh lock <dir> 
  Where: <dir>       Directory to lock 
   Like: lock-dir.sh lock . 
         lock-dir.sh lock ~/backup 
         lock-dir.sh lock /home/testuser/backup 

  unlock

  Usage: lock-dir.sh unlock <dir> 
  Where: <dir>       Directory to unlock 
   Like: lock-dir.sh unlock . 
         lock-dir.sh unlock ~/backup 
         lock-dir.sh unlock /home/testuser/backup 

  status

  Usage: lock-dir.sh status <dir> 
  Where: <dir>       Directory to show lock status 
   Like: lock-dir.sh status . 
         lock-dir.sh status ~/backup 
         lock-dir.sh status /home/testuser/backup 

  version

  Usage: lock-dir.sh version 

  help

  Usage: lock-dir.sh help 
         lock-dir.sh help COMMAND 
   Like: lock-dir.sh help lock 

```

## License

[MIT](LICENSE.md)

